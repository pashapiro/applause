import argparse
import json
import os
import sys
from ConfigParser import RawConfigParser

from tweepy import OAuthHandler

DESCRIPTION = "Authorizes Applause to access Twitter on your behalf."
CONFIG_EXISTS_WARNING = '[WARNING] Configuration file at the path specified already exists. If you continue the authorization process, you will overwrite that file. Do you wish to continue? (y/n)'
NO_OVERWRITE_MSG = 'Try using the "--credentials_file" option to specify a different filename.\nAuthorization will now exit'
INVALID_OPT_MSG = 'Invalid option. Exiting...'

def get_creds_file_path(creds_path=None):
    """
    Gets the path of the file where the new authorization
    credentials will be written.

    creds_path - The path to the new credentials file
        uses twitter/config/credentials.ini if not specified

    returns the path to the new file, or the given path if a file
    already exists there
    """
    if creds_path is None:
        creds_path = os.path.abspath(os.path.join(
            __file__,
            os.pardir,
            os.pardir,
            'config',
            'credentials.ini'
        ))

    if os.path.exists(creds_path):
        resp = raw_input(CONFIG_EXISTS_WARNING)
        if resp.lower() in ('n', 'no'):
            print NO_OVERWRITE_MSG
            sys.exit(0)
        elif resp.lower() not in ('y', 'yes'):
            print INVALID_OPT_MSG
            sys.exit(0)
        os.remove(creds_path)

    return creds_path
            

def get_args():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('--app_name', help="Your App name from Twitter")
    
    parser.add_argument(
        '--consumer_key',
        help="Your consumer key from Twitter"
    )
    
    parser.add_argument(
        '--consumer_secret',
        help="Your consumer secret from Twitter"
    )
    
    parser.add_argument(
        '--credentials_file',
        default=None,
        help="A path to the stored credentials file"
    )

    return parser.parse_args()

def main():
    args = get_args()
    consumer_key = args.consumer_key
    consumer_secret = args.consumer_secret
    app_name = args.app_name

    if app_name is None:
        app_name = raw_input("Enter your app name for Applause:")

    if consumer_key is None:
        consumer_key = raw_input("Enter your consumer key for Applause:")

    if consumer_secret is None:
        consumer_secret = raw_input("Enter your consumer secret for Applause:")
    
    creds_path = get_creds_file_path(args.credentials_file)
    
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth_url = auth.get_authorization_url()
    print 'Please go to the following URL and authorize Applause to connect to Twitter on your behalf: %s' % auth_url
    auth_pin = raw_input("Enter pin number:")
    auth.get_access_token(auth_pin)
    
    config = RawConfigParser()
    config.add_section('API Credentials')
    config.set('API Credentials', 'app_name', app_name)
    config.set('API Credentials', 'consumer_key', consumer_key)
    config.set('API Credentials', 'consumer_secret', consumer_secret)
    config.set('API Credentials', 'oauth_token', auth.access_token)
    config.set('API Credentials', 'oauth_secret', auth.access_token_secret)

    with open(creds_path, 'wb') as configfile:
        config.write(configfile)
    

if __name__ == "__main__":
    main()
