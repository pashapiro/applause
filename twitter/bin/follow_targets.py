import argparse
import os
import re
import sys
import time

from tweepy import Cursor
from tweepy.error import TweepError

sys.path.append(os.path.realpath(os.path.join(__file__, "..", "..", "lib")))

from api import TwitterSession

DESCRIPTION = "Follows the given number of real people from the given screen name's followers"
DEFAULT_BG_URL = u'http://abs.twimg.com/images/themes/theme1/bg.png'

def get_args():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument(
        'target_name',
        metavar='TARGET_NAME',
        help='The screen name of the user whose followers you want to target'
    )

    parser.add_argument(
        'target_count',
        metavar='N',
        type=int,
        help='The number of new friends to add'
    )

    return parser.parse_args()


def quality_user(tweepy_user):
    '''
        We don't want to follow bots or spam accounts. We're trying to
        get the attention of real people, so we only want to follow
        accounts that represent real people, not spam accounts or
        organization accounts or accounts that otherwise won't be
        good followers.

        Thinking: We'll determine if a user is a real person by
        analyzing certain traits of their account. Any one of these
        traits won't be a definite of whether or not they're real,
        but the more positive indicators, the more likely the user
        is real. Since we're simply trying to avoid following accounts
        that don't represent real people, it's a small mistake to skip
        a real person, but a big mistake to follow a fake one.
    '''
    real_user_pts = 10

    #Subtract points for every non-ascii character in the user's name
    num_non_ascii = len(re.sub('[\x00-\x7f]', '', tweepy_user.name))
    real_user_pts -= num_non_ascii

    #Subtract another point if the name is ONLY non-ascii characters
    real_user_pts -= 1 if num_non_ascii == len(tweepy_user.name) else 0

    #Subtract one point for every three non-ascii charactes in the
    #user's description
    des_non_ascii = len(re.sub('[\x00-\x7f]', '', tweepy_user.description))
    real_user_pts -= des_non_ascii / 3

    #Subtract another two points if the description is ONLY non-ascii characters
    real_user_pts -= 2 if des_non_ascii == len(tweepy_user.description) else 0

    #Subtract points for the non-ascii characters in their most recent tweet
    try:
        status_non_ascii = len(re.sub('[\x00-\x7f]', '', tweepy_user.status.text))
        real_user_pts -= status_non_ascii / 3
    except AttributeError, e:
        pass

    #If they don't have a description, subtract points
    real_user_pts -= 6 if len(tweepy_user.description) == 0 else 0

    #If they don't have a profile picture, subtract points
    if 'default_profile_images' in tweepy_user.profile_image_url:
        real_user_pts -= 6

    #If they don't have a background image, subtract points
    if DEFAULT_BG_URL == tweepy_user.profile_background_image_url:
        real_user_pts -= 4

    #Subtract points if they don't have a decent number of tweets
    real_user_pts -= 3 if tweepy_user.statuses_count < 500 else 0

    #Subtract points if they don't have a decent number of favourites
    real_user_pts -= 3 if tweepy_user.favourites_count < 50 else 0

    return real_user_pts > 0


def main(twitter_session, target_name, target_count):
    me = twitter_session.t.me()
    new_friends = []

    #Get the user we're targeting
    target = twitter_session.get_user_by_screen_name(target_name)

    #Get the people I'm following
    my_friends_ids = twitter_session.ids_user_follows(me.screen_name)

    #For each friend
    counter = 0
    target_cursor = Cursor(twitter_session.t.followers_ids, screen_name=target.screen_name)
    for follower_id in target_cursor.items():
        if follower_id in my_friends_ids:
            continue
        follower = twitter_session.get_user_by_id(follower_id)
        if quality_user(follower):
            try:
                twitter_session.t.create_friendship(follower.id)
                counter += 1
                print 'Friendship with %s created. Waiting...' % follower.screen_name
                new_friends.append(follower.id)
                time.sleep(6)
            except TweepError, e:
                print 'Failed to create friendship, skipping', follower.screen_name
        if counter >= target_count:
            break
        #Wait 6 seconds so we don't add more than 10 per minute
        time.sleep(6)
    return new_friends


if __name__ == "__main__":
    args = get_args()
    main(TwitterSession(), args.target_name, args.target_count)
