import os
import sys
import time

from unfollow_nonfollowers import main as unfollow
from follow_targets import main as follow
sys.path.append(os.path.realpath(os.path.join(__file__, "..", "..", "lib")))
from api import TwitterSession

start = time.time()

#Delete any recently followed files that are older than a week
#Build a list of recently followed user_ids that were followed less than a week ago
exceptions_dir = os.path.realpath(os.path.join(__file__, "..", "..", "config", "exceptions"))
week_ago = start - 604800
recent_follow_csvs = [x for x in os.listdir(exceptions_dir) if 'csv' in x]
unfollow_exceptions = []
for recent_follow_csv in recent_follow_csvs:
    file_created = float(os.path.splitext(recent_follow_csv)[0])
    if file_created < week_ago:
        os.remove(os.path.join(exceptions_dir, recent_follow_csv))
    else:
        unfollow_exceptions += open('%s/%s' % (exceptions_dir, recent_follow_csv)).read(-1).split(',')

t = TwitterSession()
#Unfollow 280 non-followers
unfollow(t, 18, unfollow_exceptions)

#Don't follow any new people if following too many people
me = t.t.me()
my_follower_ids = t.ids_following_user(me.screen_name)
my_friend_ids = t.ids_user_follows(me.screen_name)
num_friends = len(my_friend_ids)
num_followers = len(my_follower_ids)

print 'Number of followers:', num_followers
print 'Number following:', num_friends


new_friends = []
if num_friends < num_followers + 200:
    print 'Following is within threshold. Following new people...'

    #Follow Kevin Smith fans
    new_friends += follow(t, 'ThatKevinSmith', 3)

    #Follow Bill Simmons fans
    new_friends += follow(t, 'BillSimmons', 3)

    #Follow Scott Mosier Fans
    new_friends += follow(t, 'smosier', 3)

    #Follow David Shoemaker Fans
    new_friends += follow(t, 'AKATheMaskedMan', 3)

    #Follow Tyrion fans
    new_friends += follow(t, 'GoT_Tyrion', 3)

    #Follow South Park fans
    new_friends += follow(t, 'SouthPark', 3)

finish = time.time()
friends_file = open('%s/%s.csv' % (exceptions_dir, finish), 'w')
friends_file.write(','.join(str(new_id) for new_id in new_friends))
friends_file.close()

print "Daily duration:", finish - start
