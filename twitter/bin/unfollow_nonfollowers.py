import argparse
from ConfigParser import RawConfigParser
import os
import sys
import time

sys.path.append(os.path.realpath(os.path.join(__file__, "..", "..", "lib")))

from api import TwitterSession

DESCRIPTION = 'Unfollows the given number of oldest non-followers, skipping user IDs listed in exceptions.ini'

def get_exceptions_file_path():
    """
    Gets the path of the file where non-follower exceptions
    are stored. This is a list of usernames that don't need
    to follow back to continue to be followed.

    exceptions_path - The path to the new exceptions file
        uses twitter/config/exceptions.ini if not specified

    returns the path to the new file, or the given path if a file
    already exists there
    """
    return os.path.abspath(os.path.join(
        __file__,
        os.pardir,
        os.pardir,
        'config',
        'exceptions.ini'
    ))


def get_args():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument(
        'unfollow_count',
        metavar='N',
        type=int,
        help="The number of non-followers to unfollow"
    )

    return parser.parse_args()


def main(twitter_session, unfollow_count, exceptions):
    exceptions_path = get_exceptions_file_path()
    config = RawConfigParser()
    config.read(exceptions_path)

    exception_user_ids = config.get('Exceptions', 'user_ids').split(',')
    exception_user_ids += exceptions

    me = twitter_session.t.me()

    #This needs to be updated to accomodate users with more than 5000 friends
    my_follower_ids = twitter_session.ids_following_user(me.screen_name)
    my_friend_ids = twitter_session.ids_user_follows(me.screen_name)
    my_non_followers = [
        user_id for user_id in my_friend_ids 
        if user_id not in my_follower_ids and
        str(user_id) not in exception_user_ids
    ]
    to_unfollow = my_non_followers[-unfollow_count:]
    
    print 'Unfollowing %s friends' % unfollow_count
    #Unfollow the given number of users
    for user_id in to_unfollow:
        print 'Unfollowing:', user_id
        twitter_session.t.destroy_friendship(user_id)
        time.sleep(6)


if __name__ == "__main__":
    args = get_args()
    main(TwitterSession(), args.unfollow_count)
