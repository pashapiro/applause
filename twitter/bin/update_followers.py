import os, sys
sys.path.append(os.path.realpath(os.path.join(__file__, "..", "..", "lib")))
import argparse

from database import session as db, TwitterUser, Followship
from api import TwitterSession

DESCRIPTION = 'Updates the friends and followers for the given user in the database'

def get_args():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument(
        'screen_name',
        metavar='SCREEN_NAME',
        help="screen name of the user whose friends/followers will be updated."
    )

    return parser.parse_args()

#TODO: This needs update the handle changes
def main():
    #Get the username, query its data from twitter, and add it to db session
    print 'Accessing user data...'
    args = get_args()
    sesh = TwitterSession()
    user = sesh.get_user_by_screen_name(args.screen_name)
    user_model = TwitterUser(user)
    db.add(user_model)

    print 'Updating user followers...'
    #Get all of the user's followers and put them in the database
    follower_ids = sesh.ids_following_user(user_model.screen_name)
    print 'Current follower id count:', len(follower_ids)
    followers = sesh.get_users_by_id(follower_ids)
    print 'Current follower count:', len(followers)
    for follower in followers:
        follower_model = TwitterUser(follower)
        f = Followship()
        f.follower = follower_model
        user_model.follower_followships.append(f)

    print 'Updating user friends...'
    #Do the same with the user's friends (users followed by user)
    friend_ids = sesh.ids_user_follows(user_model.screen_name)
    print 'Current friend id count:', len(friend_ids)
    friends = sesh.get_users_by_id(friend_ids)
    print 'Current friend count:', len(friends)
    for friend in friends:
        friend_model = TwitterUser(friend)
        f = Followship()
        f.friend = friend_model
        user_model.friend_followships.append(f)

    db.commit()

if __name__ == "__main__":
    main()
