import itertools
import os
import sys
from ConfigParser import RawConfigParser

from tweepy import OAuthHandler, API

class TwitterSession:

    def __init__(self):
        self._init_config()
        self.auth = OAuthHandler(
            self.config.get("API Credentials", "consumer_key"),
            self.config.get("API Credentials", "consumer_secret")
        )
        self.auth.set_access_token(
            self.config.get("API Credentials", "oauth_token"),
            self.config.get("API Credentials", "oauth_secret")
        )
        self.t = API(self.auth)
    
    def _init_config(self):
        config_path = os.path.abspath(os.path.join(
            __file__,
            os.pardir,
            os.pardir,
            'config',
            'credentials.ini'
        ))
        self.config = RawConfigParser()
        self.config.read(config_path)

    def tweet(self, tweet_str):
        self.t.update_status(tweet_str)

    def get_user_by_id(self, user_id):
        return self.t.lookup_users(user_ids=[user_id])[0]

    def get_users_by_id(self, user_ids):
        id_chunks = self._grouper(100, user_ids)
        users = []
        for chunk in id_chunks:
            users = users + self.t.lookup_users(user_ids=chunk)
        return users

    def get_user_by_screen_name(self, screen_name):
        return self.t.lookup_users(screen_names=[screen_name.lower()])[0]

    def get_users_by_screen_names(self, screen_names):
        sn_chunks = self._grouper(100, screen_names)
        users = []
        for chunk in sn_chunks:
            users = users + self.t.lookup_users(screen_names=chunk)
        return users

    def ids_following_user(self, screen_name):
        return self.t.followers_ids(screen_name)

    def ids_user_follows(self, screen_name):
        return self.t.friends_ids(screen_name)

    def _grouper(self, n, iterable):
        args = [iter(iterable)] * n
        return [[e for e in t if e != None] for t in itertools.izip_longest(*args)]
