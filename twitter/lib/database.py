from datetime import datetime

from sqlalchemy import create_engine, Column, Integer, Unicode,\
DateTime, UnicodeText, Boolean, BigInteger, ForeignKey
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.schema import Table

db = create_engine('postgresql://applause:$$_ApRiL302015_$$@localhost/applause')
Session = sessionmaker(bind=db)
session = Session()

Base = declarative_base()

'''
Thoughts on modeling user relationships:

We want to be able to access all of a user's friends and followers from
that user's model. We also want to be able to add custom fields to
the actual relationships that will be accessible via the ORM. Thus, instead
of creating a Many-To-Many relationship between TwitterUser models,
we'll create separate models for Follower and Friend that have foreign
key relationships back to TwitterUser. It's on these models that we'll
be able to add custom fields to describe the relationship between users.
'''

class TwitterUser(Base):
    __tablename__ = 'twitter_users'

    id = Column(Integer, primary_key=True)
    twitter_id = Column(BigInteger)
    screen_name = Column(Unicode)
    friends_count = Column(Integer)
    followers_count = Column(Integer)
    favorites_count = Column(Integer)
    account_created_ts = Column(DateTime)
    created_ts = Column(DateTime)
    location = Column(Unicode)
    profile_has_background_img = Column(Boolean)
    verified = Column(Boolean)
    tweets_count = Column(Integer)
    status_text = Column(UnicodeText)
    followers = association_proxy('follower_followships', 'follower')
    friends = association_proxy('friend_followships', 'friend')

    def __init__(self, api_user):
        self.twitter_id = api_user.id
        self.screen_name = api_user.screen_name
        self.friends_count = api_user.friends_count
        self.followers_count = api_user.followers_count
        self.favorites_count = api_user.favourites_count
        self.location = api_user.location
        self.profile_has_background_img = api_user.profile_use_background_image
        self.verified = api_user.verified
        self.tweets_count = api_user.statuses_count
        try:
            self.status_text = api_user.status.text
        except AttributeError, e:
            self.status_text = u''
        self.created_ts = datetime.utcnow()
        self.account_created_ts = api_user.created_at


class Followship(Base):
    __tablename__ = 'followships'
    friend_id = Column(Integer, ForeignKey('twitter_users.id'), primary_key=True)
    follower_id = Column(Integer, ForeignKey('twitter_users.id'), primary_key=True)
    friend = relationship('TwitterUser', foreign_keys=[friend_id], backref='follower_followships')
    follower = relationship('TwitterUser', foreign_keys=[follower_id], backref='friend_followships')


Base.metadata.create_all(db)
